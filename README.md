# Up List
* It's 🍋 [Fresh](https://fresh.deno.dev/), it's with 🦕 [Deno](https://deno.land/)
* It's done from [Gidpod](https://gitpod.io)

## Install 🦕
Gitpod has homebrew available but curl will have the most up to date version 🤷🏻.

- Curl 👍🏼
	```sh
	curl -fsSL https://deno.land/x/install/install.sh | sh
	```
- Homebrew 👎🏻
	```sh
	brew install deno
	```

```sh
Manually add the directory to your $HOME/.bashrc (or similar)
  export DENO_INSTALL="/home/gitpod/.deno"
  export PATH="$DENO_INSTALL/bin:$PATH"
```

## Install 🍋
Beautifully simple
```sh
deno run -A -r https://fresh.deno.dev my-project
cd my-project
deno task start
```